module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "9.0.0"

  project_id   = var.gcp_project
  network_name = var.env_prefix

  subnets = [
    {
      subnet_name   = var.main_subnet_name
      subnet_ip     = var.gcp_region_cidr
      subnet_region = var.gcp_region
    }
  ]

  secondary_ranges = {
    "subnet-01" = [
      {
        range_name    = "${var.main_subnet_name}-pods-range"
        ip_cidr_range = "10.20.0.0/16"
      },
      {
        range_name    = "${var.main_subnet_name}-services-range"
        ip_cidr_range = "10.30.0.0/16"
      },
    ]
  }
}

resource "google_compute_address" "ingress_ip_address" {
  name = "ingress-ip-address"
  address_type = "EXTERNAL"
  network_tier = "PREMIUM"
  region = var.gcp_region
}

resource "google_compute_address" "ssh_ip_address" {
  name = "ssh-ip-address"
  address_type = "EXTERNAL"
  network_tier = "PREMIUM"
  region = var.gcp_region
}