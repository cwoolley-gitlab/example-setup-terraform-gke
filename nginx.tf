resource "helm_release" "nginx" {
    name  = "nginx-ingress"
    repository = "https://kubernetes.github.io/ingress-nginx"
    chart      = "ingress-nginx"
    namespace = "ingress-nginx"
    create_namespace = true
    version = var.nginx_helm_chart_version
    set {
      name = "controller.service.loadBalancerIP"
      value = google_compute_address.ingress_ip_address.address
    }
    
    depends_on = [
        module.gke
    ]

    values = [
        file("${path.module}/nginx_values.yaml")
    ]
}
