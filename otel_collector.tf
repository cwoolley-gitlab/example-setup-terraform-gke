// https://opentelemetry.io/docs/kubernetes/getting-started/
// https://opentelemetry.io/docs/kubernetes/helm/collector/

resource "helm_release" "otel_collector" {
    name  = var.agent_name
    repository = "https://open-telemetry.github.io/opentelemetry-helm-charts"
    chart      = "opentelemetry-collector"
    namespace = "otel-collector"
    create_namespace = true
    version = var.otel_collector_helm_chart_version

    values = [
      file("${path.module}/otel_collector_values.yaml")
    ]
}
