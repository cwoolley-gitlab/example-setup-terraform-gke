resource "helm_release" "gitlab_agent" {
    name  = var.agent_name
    repository = "https://charts.gitlab.io"
    chart      = "gitlab-agent"
    namespace = "gitlab-agent-${var.agent_name}"
    create_namespace = true
    version = var.agent_helm_chart_version
    set {
      name = "image.tag"
      value = var.agent_version
    }
    set {
      name = "config.token"
      value = var.agent_token
    }
    set {
      name = "config.kasAddress"
      value = var.agent_kas_address
    }
    
    depends_on = [
        module.gke
    ]
}
