# variables.tf

variable "gl_project_title" {
  description = "Passing the project title to be added to labels."
  default = "remote-development-demo-env"
}

variable "env_prefix" {
  type        = string
  description = "A short alphadash slug that is prefixed to resource names when using multiple Terraform configurations in the same GCP project."
  default     = "remote-development-demo-env"
}

variable "gcp_dns_zone_name" {
  type        = string
  description = "The GCP Cloud DNS zone name for this environment. This is automatically generated and set in the GitLab Project CI variables."
}

variable "gcp_project" {
  type        = string
  description = "The GCP project ID that the resources will be deployed in. This is automatically generated and set in the GitLab Project CI variables."
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in. (Ex. us-east1)"
  default     = "us-central1"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone that the resources will be deployed in. This must match the region. (Example: us-east1-c)"
  default     = "us-central1-c"
}

variable "gcp_region_cidr" {
  type        = string
  description = "A /12 CIDR range for the GCP region that will be used for dynamically creating subnets. You can leave this at the default value for most use cases. (Ex. 10.128.0.0/12, 10.144.0.0/12, 10.160.0.0/12)"
  default     = "10.128.0.0/12"
}

variable "gcp_machine_type" {
  description = "the gcp machine type"
  default     = "e2-medium"
}

variable "labels" {
  type        = map(any)
  description = "Labels to place on the GCP infrastructure resources. Label keys should use underscores, values should use alphadash format. No spaces are allowed. This is automatically generated and set in the GitLab Project CI variables."
  default = {}
  # default = {
  #   owner = "support-team",
  # }
}

# -----------------------------------------------------------------------------
# Add your Terraform custom variables below this line
# You can define non-sensitive values in terraform.tfvars.json
# You can define sensitive values in GitLab CI variables with `TF_VAR_var_name`.
# -----------------------------------------------------------------------------



# -----------------------------------------------------------------------------
# Add your Terraform custom variables above this line
# -----------------------------------------------------------------------------

variable "main_subnet_name" {
  type = string
  default = "subnet-01"
}

variable "cluster_zones" {
  type = list(string)
  default = ["us-central1-a", "us-central1-b", "us-central1-f"]
}

variable "cluster_node_locations" {
  type = string
  default = "us-central1-b,us-central1-c"
}

variable "nginx_helm_chart_version" {
  type = string
  default = "4.10.0"
}

variable "agent_token" {
  type = string
  sensitive = true
}

variable "agent_version" {
  type = string
  default = "v16.9.3"
}

variable "agent_kas_address" {
  type = string
  default = "wss://kas.gitlab.com"
}

variable "agent_name" {
  type = string
  default = "tanuki"
}

variable "agent_helm_chart_version" {
  type = string
  default = "1.24.0"
}

variable "workspaces_proxy_name" {
  type = string
  default = "gitlab-workspaces-proxy"
}

variable "workspaces_proxy_client_id" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_client_secret" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_auth_host" {
  type = string
  default = "https://gitlab.com"
}

variable "workspaces_proxy_redirect_uri" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_signing_key" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_ingress_class_name" {
  type = string
  default = "nginx"
}

variable "workspaces_proxy_helm_chart_version" {
  type = string
  default = "0.1.12"
}

variable "workspaces_proxy_workspace_domain_cert" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_workspace_domain_key" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_wildcard_domain_cert" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_wildcard_domain_key" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_ingress_host_workspace_domain" {
  type = string
}

variable "workspaces_proxy_ingress_host_wildcard_domain" {
  type = string
}

variable "workspaces_proxy_ssh_host_key" {
  type = string
  sensitive = true
}

variable "workspaces_proxy_log_level" {
  type = string
  default = "info"
}

variable "otel_collector_helm_chart_version" {
  type = string
  default = "0.85.0"
}
