# example-setup-terraform-gke

## Setup GCP Account

1. [Create a service account](https://cloud.google.com/iam/docs/service-accounts-create) in GCP
    1. Enable the IAM API for your project:
    1. Follow instructions in https://cloud.google.com/iam/docs/service-accounts-create#console
        1. Pick `Owner` for the service account role
1. Open the new Service Account in the Cloud Console
1. [Create access keys](https://cloud.google.com/iam/docs/keys-create-delete) for the service account:
    1. Go to the Keys tab and add a new key
    1. Pick `JSON` key type
    1. Save the key in a secure location
1. Enable Kubernetes Engine API
    1. In the Cloud Console menu, go to `Kuberenetes Engine`
    1. Click `Enable`
1. Enable Cloud Resource Manager API
    1. In the Cloud Console menu, go to `Marketplace`
    1. Search for `Cloud Resource Manager API` and select it
    1. Enable it

## Set up requirements for Remote Development cluster

1. Ensure you have enabled the DNS API: https://cloud.google.com/dns/docs/set-up-dns-records-domain-name
1. Obtain a DNS zone Name
    1. Follow instructions to [register a cloud domain](https://cloud.google.com/domains/docs/register-domain)
    1. This will automatically set up the Cloud DNS for the new domain

## Set up GitLab Agent for Kubernetes

1. You will need to pick a group and project to which you have Maintainer access.
   **_Projects under personal namespaces are not yet supported._**
1. Follow the instructions to [Create an agent configuration file](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#create-an-agent-configuration-file). Here is [an example](https://gitlab.com/gitlab-org/remote-development/testing/gitlab-agent-configurations/-/blob/main/.gitlab/agents/z-internal-ide-team-testing/config.yaml?ref_type=heads).
    1. Note that all agents will be listed alphabetically in the Workspace create screen. So if you are making an agent
       in a shared group but it is only for personal testing, start the name with `z-` so it will be at the end of the
       list.
    1. In the `config.yaml` file add appropriate contents. See [these docs](https://docs.gitlab.com/ee/user/workspace/gitlab_agent_configuration.html)
       for an example. Here is a minimal example - replace `<DNS ZONE ...>` with `workspaces.` + the full domain name you
       registered above:
       ```yaml
       remote_development:
         enabled: true
         dns_zone: <DNS ZONE - e.g. workspaces.infrastructure-development-gke.developmentsandbox.net>

       # for debugging locally to increase log verbosity
       observability:
         logging:
           level: debug
           grpc_level: warn
       ```
1. Follow the instructions to [Register the agent with GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab)
1. Do not do the `Install the agent in the cluster` step. This will be done via setting the CI Pipeline ENV vars below.

## Set up the Workspaces Proxy

1. Follow the [`gitlab-workspaces-proxy` setup instructions](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy#installation-instructions), with these differences:
    1. Stop when you get to the section for `Create configuration secret for the proxy and deploy the helm chart`.
       Instead, you will set the values via the CI pipeline ENV vars below.
    1. To set up the DNS records, go to the Cloud DNS page for your new domain in the Cloud Console.

## Setup CI Pipeline

1. Fork the repository.
1. Go to your fork of this project in the GitLab UI
1. Define terraform environment variables as CI pipeline variables in the forked project:
    1. Go to `Settings -> CI/CD -> Variables -> Expand`
    1. Add all the variables defined in the table below. List of variables is available at [variables.tf](./variables.tf).
    1. **MAKE SURE YOU DON'T HAVE ANY TRAILING NEWLINES WHEN YOU PASTE THE VARIABLE VALUES!**

| Name | Note |
|-|-|
| GOOGLE_APPLICATION_CREDENTIALS | `TYPE=file` - The JSON keys file you saved above |
| TF_VAR_gcp_dns_zone_name | The name from the Cloud DNS name registered above |
| TF_VAR_gcp_project | Name of the project, e.g. `<username>-xxxxx` |
| TF_VAR_agent_token | Token you saved when creating the GitLab Agent for Kubernetes agent |
| TF_VAR_agent_kas_address | Required if your GitLab instance is not gitlab.com |
| TF_VAR_workspaces_proxy_client_id | The GitLab OAuth application id you created |
| TF_VAR_workspaces_proxy_client_secret | The GitLab OAuth application secret you created |
| TF_VAR_workspaces_proxy_auth_host | Required if your GitLab instance is not gitlab.com |
| TF_VAR_workspaces_proxy_redirect_uri | `REDIRECT_URI` from the proxy setup - will be in ENV var|
| TF_VAR_workspaces_proxy_signing_key | `SIGNING_KEY` from the proxy setup - will be in ENV var |
| TF_VAR_workspaces_proxy_workspace_domain_cert | Copy via `cat $WORKSPACES_DOMAIN_CERT \| pbcopy` |
| TF_VAR_workspaces_proxy_workspace_domain_key | Copy via `cat $WORKSPACES_DOMAIN_KEY \| pbcopy` |
| TF_VAR_workspaces_proxy_wildcard_domain_cert | Copy via `cat $WILDCARD_DOMAIN_CERT \| pbcopy` |
| TF_VAR_workspaces_proxy_wildcard_domain_key | Copy via `cat $WILDCARD_DOMAIN_KEY \| pbcopy` |
| TF_VAR_workspaces_proxy_ingress_host_workspace_domain | Copy via `echo $GITLAB_WORKSPACES_PROXY_DOMAIN \| pbcopy` |
| TF_VAR_workspaces_proxy_ingress_host_wildcard_domain | Copy via `echo $GITLAB_WORKSPACES_WILDCARD_DOMAIN \| pbcopy` |
| TF_VAR_workspaces_proxy_ssh_host_key | Copy via `cat $SSH_HOST_KEY \| pbcopy` |
| TF_VAR_env_prefix | Required if you have multiple demos in same GCP project, optionally override to something different |

## Run CI Pipeline to create the cluster

1. Go to your fork of this project in the GitLab UI
1. Go to `Build -> Pipelines` and run the pipeline
1. Ensure everything in the `plan` job looks right
1. Manually run the `deploy` job

### Add DNS records

TODO: Update terraform config to do this automatically.

1. Inspect the output of the `apply` job.
    ```
    ingress_ip_address = "INGRESS_IP_ADDRESS"
    ssh_ip_address = "SSH_IP_ADDRESS"
    ```
1. In the Cloud Console, add the following for your Cloud DNS domain, using the values of the vars from above as the record names:
    1. Record of type A for `<TF_VAR_workspaces_proxy_ingress_host_workspace_domain>` to `<INGRESS_IP_ADDRESS>`.
    1. Record of type A for `<TF_VAR_workspaces_proxy_ingress_host_wildcard_domain>` to `<INGRESS_IP_ADDRESS>`.
    1. Record of type A for `ssh.<TF_VAR_workspaces_proxy_ingress_host_workspace_domain>` to `<SSH_IP_ADDRESS>`.

## Access the cluster

1. See https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl
1. NOTE: Use `kubectx` to switch back to a different kubernetes context if needed.

### Kuberentes context

Follow https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl#install_plugin to fetch the context of the created Kubernetes cluster.


## Installation under a GitLab Sandbox Cloud account

1. Get a Cloud Sandbox account and a GCP project as described in https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project
1. Access the GCP Project and go to the GCP Cloud Console for the project
1. Follow the instructions above under #usage
