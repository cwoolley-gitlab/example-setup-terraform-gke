# providers.tf

terraform {
  backend "http" {
    # See .gitlab-ci.yml jobs that use terraform-init -backend-config variables.
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "5.18.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "5.9.0"
    }
  }
  required_version = ">= 1.0.5"
}

# Define the Google Cloud Provider
provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
  zone    = var.gcp_region_zone
}

# Define the Google Cloud Provider with beta features
provider "google-beta" {
  project = var.gcp_project
  region  = var.gcp_region
  zone    = var.gcp_region_zone
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = "https://${module.gke.endpoint}"
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(module.gke.ca_certificate)
  }
}